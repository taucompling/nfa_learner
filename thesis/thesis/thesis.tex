\documentclass[12pt]{report}
\usepackage{import}
\usepackage{epsfig}
\usepackage{float}
\usepackage{url}
\usepackage{amsmath}
\usepackage{longtable}

\setlength{\textwidth}{6.5in}
\setlength{\textheight}{8in}
\setlength{\LTcapwidth}{6in}

\setlength{\oddsidemargin}{0.in}

\newcommand{\doublespaced}{\renewcommand{\baselinestretch}{1.5}\normalfont}
\newcommand{\halfspaced}{\renewcommand{\baselinestretch}{1.2}\normalfont}
\newcommand{\singlespaced}{\renewcommand{\baselinestretch}{1}\normalfont}
\renewcommand{\thefigure}{\thechapter.\arabic{figure}}

\makeglossary

\begin{document}
\thispagestyle{empty}
\begin{center}
\fontsize{16}{16}
\selectfont
\halfspaced
Tel-Aviv University\\
The Raymond and Beverly Sackler Faculty of Exact Sciences\\
Blavatnik School of Computer Science\\

\vspace{25 mm} \doublespaced \fontsize{22}{22} \selectfont \textbf{Predicting harmful mutations by learning regular grammars of protein sequences}

\halfspaced
\vspace{25 mm}
\fontsize{16}{16}
\selectfont
This thesis is submitted in partial fulfillment\\
of the requirements towards the M.Sc. degree\\
Tel-Aviv University\\
Blavatnik School of Computer Science\\

\vspace{20 mm}
\fontsize{12}{12}
\selectfont
by\\

\fontsize{16}{16} \selectfont Uri Valevski

\vspace{10 mm}
\fontsize{12}{12}
\selectfont
The research work in this thesis has been carried out\\
under the supervision of Prof. Roded Sharan.

\vspace{10 mm}

July, 2015
\end{center}

\newpage

\thispagestyle{empty}
\fontsize{12}{12}
\selectfont

\newpage

\pagenumbering{roman}

\fontsize{12}{12} \selectfont
\doublespaced

\begin{abstract}

\subimport{../}{abstract}

\end{abstract}

\pagenumbering{Roman}
\setcounter{tocdepth}{2}
\tableofcontents

\chapter{Introduction} \label{chp:introduction}
\pagenumbering{arabic}

\subimport{../}{introduction}

The thesis is organized as follows:
Chapter 2 gives some biological and computational background,
Chapter 3 presents the method itself
and Chapter 4 presents the experimental results,
along with a comparison to other known methods.
Finally, a summary and future research ideas appear in Chapter 5.

\chapter{Background} \label{chp:background}

This chapter provides computational and biological background necessary for the presented work.
The computational background covers some important concepts from computer science theory used in this thesis.
The biological background provides an introduction to protein mutation sequence analysis.

\section{Computational Background}

\subsection{Formal Language Theory and Non Deterministic Finite Automaton}
A formal language is a (usually infinite) set of strings over a finite alphabet, also referred to as words. One can speak of an {\em acceptor} for a language, that is an algorithm that given a string can verify if it is part of the language or not. In an intuitive sense, the acceptor contains the essence of the language in finite space. Different acceptors can be divided into hierarchies by looking at the computational model that is required to run them. Usually, and also in our case, the acceptor is also a {\em generator} (also referred to as {\em grammar} at times), meaning that its function can be reversed and when given different inputs it can produce all words in the language. The process of running the acceptor on a given string to decide if it is part of the language is called {\em parsing}.

We will look at the set of protein sequences as a formal language that abides to certain rules, and therefore has some (unknown) acceptor/generator associated with it. Presumably, knowing this model would enable us to know if a mutation is acceptable or not, i.e. does it create a word which is not part of the language.

The family of languages chosen for this work is that of regular grammars, for which there exists an acceptor/generator called the deterministic finite automaton~\cite{Rabin_1959} ({\em DFA}). A DFA is a finite-memory computational model that parses strings in linear time. DFAs accept exactly the set of regular languages.

A DFA is composed of a graph of states and annotated transitions between them. Each state has transitions leading out to a next state for any specific input (a letter from the language alphabet). Upon reading a symbol, a DFA jumps deterministically from a state to another by following the transition arrow. A DFA has a start state and a set of accept states (denoted graphically by a double circle) which help define when a computation is successful.

The non-deterministic version of a DFA is called nondeterministic finite automaton ({\em NFA}). It is equivalent the DFA representation (both can accept/generate the same family of languages, both have linear time parsing algorithms) but encoded differently. The main difference between the two versions is that the NFA can transition to more than one state after reading a symbol, so outgoing edges with the same symbol can appear, and parsing works with a set of active states instead of one active state at any given time. The NFA formulation is the one used in this work. The model and parsing algorithm are described thoroughly in Chapter~\ref{sec:nfa}.

\begin{figure}[!ht]
  \includegraphics[width=\textwidth]{protein-deps.png}
  \caption{Protein structure. Taken from~\cite{formallanguage}}
\label{deps-figure}
\end{figure}

\begin{figure}[!ht]
  \includegraphics[width=\textwidth]{nested-deps.png}
  \caption{Nested dependencies. Taken from http://lewisdartnell.com/}
\label{nested-deps}
\end{figure}

\begin{figure}[!ht]
  \includegraphics[width=\textwidth]{cross-deps.png}
  \caption{Cross dependencies}
\label{cross-deps}
\end{figure}

The choice of regular grammars as the model for protein sequences is due the linear time parsing algorithm. However, it should be noted that it is not the most natural choice. The common notion about protein sequences is that they cannot be adequately described using regular grammars, as they contain cross-serial and nested dependencies~\cite{formallanguage} (Figure~\ref{deps-figure}). Nested dependencies are long range dependencies which require a stack to parse, as elements are resolved in reverse order than they are scanned (Figure~\ref{nested-deps}). Cross-serial dependencies occur when the lines representing the dependency relations between two parts of the string cross over each other (Figure~\ref{cross-deps}). These require more sophisticated computational models.

\subsection{Inductive Inference Theory and The Minimum Description Length Principle}
The first stage of this work involves learning a generating grammar for protein sequences.
We do this in an unsupervised manner, i.e. we present to our algorithm the sequences themselves and nothing more.
When we search for a good hypothesis (the generating grammar) we rely on the minimum description length principle ({\em MDL})~\cite{mdl}.

The MDL paradigm emerges from the work done by Ray Solomonoff and Andrey Kolmogorov in developing their theory of inductive inference. Their work can be thought of as a formal incarnation of two intuitive notions about induction; (i) that all models that fit the data are plausible (Epicurus' principle of multiple explanations), and (ii) that the relative importance of each model is proportional to its length of encoding in bits (Occam's razor).

The mathematical object that embodies these notions is the universal probability distribution $m$ which assigns a probability for binary strings: $m(x) := \sum_{p:U(p)=x} {2^{-|p|}}$ where $p$ is a binary string encoding a prefix Turing machine that produces $x$ and $U$ is a universal prefix Turing machine that runs $p$. A prefix Turing machine reads from a unidirectional input tape and writes to a unidirectional output tape, while having additional bidirectional tapes for internal state. The inputs to $U$ are pairs of an encoded machine and some input. The prefix property is important, as it implies that the various inputs given to $U$ on which $U$ halts, form a prefix-free set, meaning that no valid input is the prefix of any other input. Therefore using Kraft's inequality~\cite{kraft}, we are assured that $\sum_{x} {m(x)} \leq 1$. Intuitively, the probability assigned to strings can be thought of as the chance that the universal prefix Turing machine produces $x$ at some point when reading from an input tape composed of potentially infinite number of coin flips.

Note that the most significant element of the summation is the Kolmogorov Complexity~\cite{Kolmogorov_1968}, $K(x)$, which is defined as the length in bits for some encoding of the shortest Turing machine that produces a certain string. $-\log m(x) = K(x) + O(1)$.

What justifies calling $m$ `universal' is that the specific choice of $U$ and how $p$ is decoded is unimportant, as it is possible to translate between different machine encodings with the addition of a constant factor. Furthermore, it is possible to prove $m$ is the best function of its type\footnote{$m$ is an element of the family of lower semi-computable (functions that can be approximated from below) semi-measures (measures with the additivity condition relaxed to super-additivity).}, in the sense that if we have another function which gives higher probabilities to some strings, $m$ will perform similarly with the addition of a constant amount of evidence\footnote{This stems from the fact that $m$ can be shown to multiplicatively dominate every other element in its family, i.e. that for any other function of this family, $p$, there exists a constant $c \geq 0$ such that $m(x) \geq cp(x)$. Specifically $c$ can be taken as $2^{-K(x)}$. This constant addition of bits can be thought of as extra evidence. See complete proof in~\cite{Li_1997}.}. In principle, any given probability distribution of a data set can be used to build a code which compresses it. Simply assign each string with a code of length equal to the log of its probability. Since in MDL we identify learning and compression, as each learned regularity can be used to compress data and vice versa, $m$ gives the most effective compression and therefore the best explanation of the data.

As might be expected, this code and probability distribution are incomputable (even the most significant element of the summation, the Kolmogorov complexity is incomputable). Therefore certain relaxations must be made to utilize this theory in empirical science:
\begin{enumerate}
  \item We are interested only in models for a specific group of strings (protein sequences in this case).
  \item We limit ourselves to a weaker family of models rather than Turing machines, so we can easily know if $U(p)=x$ (regular grammars in this case).
  \item We don't search for all possible $p$, but rather only search for the shortest one, comprising of the most substantial element of the summation.
\end{enumerate}

\subsection{Supervised Learning and Decision Trees}
Applying machine learning techniques on data comprised of labeled samples is called {\em supervised learning}. The aim is to train a model that given an unseen sample yields the correct label for it. This work utilizes both supervised and unsupervised learning. In the supervised learning part we apply the decision tree algorithm.

A decision tree~\cite{Gordon_1984} is a supervised learning algorithm which recursively builds a binary tree to be used as a classification model. Given a sample, the tree is traversed, turning to the left or right child according to the conditions in each node (whether a certain variable of the sample is above or below a certain threshold), finally reaching a leaf node which determines the given class.

To construct the tree in the training phase, a recursive greedy training algorithm is applied. It adds nodes to the tree until a certain depth or stopping condition have been reached. To add a node, all possible splits of the samples in this node are examined. The selected split is the one that best reduces the impurity of the data when comparing parent and child nodes.

Impurity is intuitively the amount of diversity of the samples at a certain node. If they are homogeneous, i.e. generally have the same label, the impurity is small. Formally this can be estimated using different criteria, for example the Gini criterion used here. The Gini criterion for two equally important and equally frequent labels is $i(t) = p(A|t)p(B|t)$, where $i$ is the impurity measure, $A$ and $B$ are the two labels and $t$ is the examined node. The splitting criterion follows as $\Delta i(s, t) = i(t) - p_L i(t_L) - p_R i(t_R)$ where $s$ is the examined split, $t_L$, $t_R$ are the left and right children of the split and $p_L$, $p_R$ are the probabilities to reach each child (determined by the amount of samples flowing into each node divided by those in the parent node).

\section{Biological Background}

\subsection{Secondary Structure}
A protein in its initial form is a chain of amino acids bound by covalent bonds, also called a polypeptide chain. Secondary structure refers to the next phase where the polypeptide chain undergoes local foldings which occur due to hydrogen bonds formed between proximate amino acids. There are relatively few recurring patterns of secondary structure, where the most common ones are alpha helices and beta sheets.

The secondary structure of a protein is a precursor to its final structure, the final atomic positioning, also called the tertiary structure, which is reached by further folding. Some proteins connect with others to create a multi-subunit complex, called quaternary structure. These further alterations are driven by the physiochemical properties of amino acids, such as charge, hydrophobicity/hydrophilicity, size and functional groups (sub-molecules responsible for what chemical reactions the molecule takes part in), which determine what residues bind together, and whether residues are exposed/internal in the final structure.

\subsection{Structural Classification of Proteins (SCOP)}

The Structural Classification of Proteins ({\em SCOP}) database is a largely manual classification of protein structural domains based on similarities of their structures and amino acid sequences. The hierarchy of SCOP: Class $>$ Fold $>$ Superfamily $>$ Family $>$ Protein Domain.

Domains are taken as the basic unit of classification, and can be thought of as a complete module or subunit inside a protein. Small proteins and most medium sized usually consist of just one domain. Domains repeat in different proteins and different species. The first hierarchy above domains are families. A family consists of domains with the same shape and some similarity of sequence and/or function, which are assumed to have a closer common ancestor. A group of families is called a superfamily and contains domains which are assumed to have at least a very distant common ancestor. Above superfamilies are folds. Domains belonging to the same fold have the same major secondary structures in the same arrangement with the same topological connections. At the top of the hierarchy are classes, which divide the domains according to their secondary structure composition, for example domains composed only of alpha helices, or beta sheets or certain types of combinations etc.

\subsection{Protein compressibility}
Proteins contain recurring subsequences (secondary structure motifs for example) that adhere to certain constraints, governing folding and functionality. It is therefore plausible that protein sequences would be compressible to a significant extent.

Historically, protein sequences, as opposed to DNA, have a reputation of being quite difficult to compress. Most standard text compression algorithms do not pass the 4 bits-per-symbol (bps) mark, where a trivial representation uses $log 20$ or 4.3 bps. This can be explained by the little Markov dependencies found in them~\cite{incompressible}. Other algorithms do a considerably better job at compressing entire proteomes, using the observation that proteomes contain exact repeats of large sequences or entire genes, sometimes in the same order~\cite{compressible}.

\subsection{Mutation types}
DNA mutation is a permanent change of the DNA sequence. It occurs due to molecular decay (spontaneous molecular changes to nucleotide molecules), errors in processes of replication and DNA repair, and external causes such as radiation. The types of mutations include insertion, deletion and replication of base pairs.

A missense mutation is one that changes the amino acid the DNA sequence encodes. This is not always the case as there is some redundancy in the DNA code for amino acids, causing some mutations to be silent (they do not alter the resulting protein).

A nonsense mutation is one that alters a triplet of nucleotides from one that code for an amino acid to a stop codon. Stop codon end the translation process, causing the truncation of the resulting protein.

Since the reading frame for coding DNA sequences is composed of 3 base pairs, when the mutation includes deletion or insertion of a number non divisible by 3, a frame-shift occurs, and the resulting protein will be completely altered, usually resulting in a non functional protein.

\subsection{Mutation Analysis}
The problem of differentiating between negative, or deleterious mutations and neutral ones has a long research history. Among the leading current methods that address this problem, are SIFT\cite{sift}, PolyPhen-2\cite{polyphen} and PROVEAN\cite{provean}. These methods share a strategy which is primarily based on protein homologs. Homologs are slightly altered versions of the same protein occurring in different species. The underlying assumption is that when an amino acid at a certain position is preserved across homologs, it is less tolerant to mutations and vice versa. The three algorithms use sequence alignment methods to find homologous sequences in other species and then examine the distribution of amino acids to score the tolerance of the query position to mutations. Aside from homology information other feature types are used, but are much less predominant and only slightly increase their performance. These include structural annotations, sequences information and protein-protein interactions.

% http://genetics.bwh.harvard.edu/pph2/dokuwiki/appendix_a 

% http://genetics.bwh.harvard.edu/pph2/dokuwiki/_media/nmeth0410-248-s1.pdf

PolyPhen-2 uses the PSIC (Position-Specific Independent Counts) score as the main feature of the method. Given a set of aligned sequences the PSIC algorithm returns a profile matrix. The elements of this matrix are the logarithmic ratio of the likelihood of a specific amino acid occurring at a particular position to the likelihood of this amino acid occurring at any position (background frequency). The feature set includes the score for the original amino acid, the substitution and the difference between the two scores as 3 individual features. Other features are employed as well, such as nucleotide sequence context features (CpG for example), and structural features, but according to our analysis the main performance of the method is due to the use of the PSIC score.

Whereas PolyPhen-2 includes other features, PROVEAN is completely based on sequence alignment, yielding similar results. PROVEAN uses a `delta alignment score', which given a sequence, a mutation and a homologue, computes how the mutation affected the similarity between the sequence and its homologue. The similarity computation is based on a given amino acid substitution matrix which lists the probabilities of an amino acid to be swapped with one another (BLOSUM matrix). The PROVEAN algorithm works in the following manner: Using alignment, it selects homologous and distantly related sequences. The sequences are clustered based on a sequence identity to remove redundancy. The algorithm uses a constant number of the top sequence clusters by similarity to the query sequence. Inside each cluster, the delta alignment score is computed for each sequence with the query sequence, and averaged over the cluster. The average scores of the clusters are then averaged again so that each cluster is weighted equally, constituting the final score.

SIFT aligns the query sequence with similar sequences from a protein database. It then scans each position in the alignment and calculates the probabilities for the various amino acids at that position, normalized by the probability of the most frequent amino acid. This constitutes the SIFT score that is used to predict if the mutation is deleterious or not.

\subsection{The Uniprot HUMSAVAR database}
To test our algorithm we used the UniProt human polymorphisms and disease mutations (HUMSAVAR) database which contains non synonymous single nucleotide polymorphisms ({\em nsSNP}): 26,508 mutations associated with diseases and 38,099 neutral mutations.

A SNP is a mutation in a single nucleotide between members of one species. Among all types of sequence polymorphisms, SNPs are the most common one, occurring on average about every 100 to 300 bases. They are considered the major source of heterogeneity and account for 90\% of all sequence variation.

Some SNPs occur at coding regions, i.e. inside genes. As there is some redundancy in the DNA code, the mutation does not always cause a different amino acid to occur, but when does it is called a non synonymous SNPs. In these cases the change may result in gain or loss of function of the resulting protein. Alternatively, they can be silent, or neutral mutations, having no apparent phenotype.

\chapter{Algorithmic Approach} \label{chp:algorithm}

\subimport{../}{algorithm}

\chapter{Experimental Results} \label{chp:results}

\subimport{../}{results}

\chapter{Conclusions} \label{chp:conclusions}
To summarize the contributions in this thesis, a novel grammar-based approach to modeling mutations and predicting their effects has been designed which outperforms state-of-the-art approaches that use conservation data.

Furthermore we have found that predicted structural annotations combined with amino acid categorization exposes meaningful regular dependencies of protein sequences, as shown by the compression rate achieved by our regular model, and the performance in the task of identifying deleterious mutations.

Additionally, we applied the Words2Vec algorithm on protein sequences and found it to produce meaningful representations of amino acids.

We speculate that two possible research directions could extend this proof-of-concept study:
(i) Applying the same technique with models from formal language theory other than regular grammars,
such as context free grammars, may improve its performance.
(ii) As this framework produces general features in unsupervised manner, the same method could be applied to other sequence analysis problems.

\newpage

\bibliographystyle{splncs}
\bibliography{refs}

\end{document}