\contentsline {chapter}{\numberline {1}Introduction}{1}
\contentsline {chapter}{\numberline {2}Background}{3}
\contentsline {section}{\numberline {2.1}Computational Background}{3}
\contentsline {subsection}{\numberline {2.1.1}Formal Language Theory and Non Deterministic Finite Automaton}{3}
\contentsline {subsection}{\numberline {2.1.2}Inductive Inference Theory and The Minimum Description Length Principle}{5}
\contentsline {subsection}{\numberline {2.1.3}Supervised Learning and Decision Trees}{8}
\contentsline {section}{\numberline {2.2}Biological Background}{9}
\contentsline {subsection}{\numberline {2.2.1}Secondary Structure}{9}
\contentsline {subsection}{\numberline {2.2.2}Structural Classification of Proteins (SCOP)}{9}
\contentsline {subsection}{\numberline {2.2.3}Protein compressibility}{10}
\contentsline {subsection}{\numberline {2.2.4}Mutation types}{10}
\contentsline {subsection}{\numberline {2.2.5}Mutation Analysis}{10}
\contentsline {subsection}{\numberline {2.2.6}The Uniprot HUMSAVAR database}{12}
\contentsline {chapter}{\numberline {3}Algorithmic Approach}{13}
\contentsline {section}{\numberline {3.1}Model}{13}
\contentsline {section}{\numberline {3.2}Grammar Induction}{14}
\contentsline {section}{\numberline {3.3}Supervised Learning Features}{18}
\contentsline {chapter}{\numberline {4}Experimental Results}{20}
\contentsline {chapter}{\numberline {5}Conclusions}{25}
