The first method consists of unsupervised learning of a generative model for the sequence data, aimed at producing general descriptive features.

\subsubsection{Model} \label{sec:nfa}
Considering that it is a sequence that defines a protein, i.e. an ordered set of symbols with varying length, a generative model from formal language theory is a natural choice for the unsupervised learning task. Such models have been used before for various sequence analysis tasks~\cite{formallanguage,recentlinguistic}. One of their advantages is alleviating the need to coerce data into fixed length, for example by looking at a fixed window (like in \cite{neuralnet}), and thus prevent potential signal loss in cases where long range dependencies exist. For performance reasons, the nondeterministic finite automaton (NFA) model is used.

An NFA can be represented as a graph, where each node is a state (including an initial state). At each step, a subset of the states are active. Initially, the only active state is the initial state. Transitions between states are represented as directed edges in this graph. A transition is associated with a symbol which could be any character from the sequence alphabet or one of two additional special symbols; wild card ($.$) and epsilon transition ($\epsilon$).

A parse of a sequence is built iteratively by scanning the sequence, one symbol at a time. At the beginning of each step, states which are connected to active states via epsilon transitions are made active as well. Then the set of active states is transformed by taking all valid transitions from currently active states to neighboring states. A transition is valid if it is associated with the current symbol or with the wild card symbol. When the last symbol has been scanned, all possible paths traversed to reach the final set of active states are returned as parses, otherwise an empty set is output.

To compute the description length of the NFA, it is encoded as follows:
\begin{enumerate}
  \item Let $s$ be the number of states in the model, $s+1$ bits are required to represent each state (1's followed by a single 0).
  \item Let $t$ be the number of symbols in the alphabet and $r$ be the number of transitions in the model, 
        $r(2\log(s) + \log(t))$ bits are required to represent the transitions.
  \item A parse is encoded as a series of transitions. Each transition from state $i$ requires $\log(r_i)$ bits, where $r_i$ is the number of outgoing transitions from state $i$.
\end{enumerate}

\subsubsection{Grammar Induction}
In the unsupervised learning stage the minimum description length (MDL) principle is used; the best model for given data is the one which best compresses it~\cite{mdl}. The compression, or description length (in bits) of the data consists of two parts: (i) The description length of the model and (ii) the description length of the data set given the model. Given the NFA, one could use a parse to reconstruct the original sequence. If the NFA binary code and the parse binary code are smaller in length than what is required to store the original sequence, then the code is a form of compression.

To search for a model that best compresses the data we apply a simulated annealing search with the description length as the optimization criterion. The annealing process begins with a simple NFA. It consists of one state, with one transition per symbol to itself. This initial NFA would be able to produce any string, but the parse lengths would be almost identical to a trivial bit representation of a string over an alphabet of 20 letters (with a small addition of the code for the NFA itself).

At each step of the process one of the following transformations is randomly selected and applied on the current model to receive a neighboring model:
\begin{enumerate}
  \item Replace a random symbol: Choose a transition between two states and replace its associated symbol.
  \item Add a random state: Add a state and two transitions, one going in and one exiting from the new state.
  \item Remove a random state: Remove an existing state and all its associated transitions.
  \item Add a random transition: Pick two states and form a new transition between them.
  \item Remove a random transition: Pick a random transition and remove it.
\end{enumerate}

After producing the neighbor, it is examined. If it provides better compression, we move to it and continue the search. If not, we might still move to it with probability $e^\frac{-\Delta E}{\tau}$, where $E$ is the measure of compression, i.e. the code length for the NFA representation added to the code length of its parses of the training set. $\Delta E$ is the difference between the performance of the current NFA and the neighboring NFA. $\tau$ stands for the temperature which is gradually reduced at each iteration.

In the case where the examined neighbor grammar cannot parse one or more protein sequence, we do not move to it. One could imagine a penalty instead of completely ruling the neighbor out, but in our experience this leads to deep local minimas the annealing process has trouble escaping from.

As each iteration requires using the examined NFA to parse the entire training set, the procedure is quite costly, even with a relatively simple model such as the NFA. In order to minimize the run time, representative sequences from SCOP domains were used as a training set rather than the entire set of known proteins. With this training set one iteration takes a few seconds on a standard PC, resulting in total runtime of the unsupervised phase of about 24 hours (for 10,000 iterations). Before being fed to the unsupervised learner, the sequences undergo translation to the MLA18SS alphabet (see \ref{mla18ss}).

For the simulated annealing temperature cooling schedule the following formula, suggested by \cite{cooling}, was employed: $\tau = c / \log(1 + iteration)$, where the constant $c$ should be greater than the deepest local minimum of the search space which is not a global one. Due to the a-priori selection of 10,000 iterations our various initial choices for $c$ tended to miss the area of temperatures where most learning seems to occur, by either passing it fast or not reaching it in time. To solve this we add a constant, $k$, to allow a more gradual decrease of the temperature which we found to be useful, resulting in the following cooling schedule: $\tau = c / \log(1 + \frac{iteration}{k})$. $k$ was selected as 1,000 and $c$ as 300 through trial and error to yield best results when using 10,000 iterations.