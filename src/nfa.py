import copy
import json
import sys
import random

import utils

inf = float('inf')

class Sparse3DMatrix():
  def __init__(self, default_value=False, shape=[5, 5, 5]):
    assert(len(shape) == 3)
    self._shape = shape
    self._init_non_empty_z()
    self._default_value = default_value
    self._elements = {}
  # Gets the item stored or the default value if it is missing.
  def __getitem__(self, triplet):
    return self._elements.get(triplet, self._default_value)
  def __setitem__(self, triplet, value):
    if value == self._default_value:
      if triplet in self._elements:
        self._delete_element(triplet)
    else:
      if triplet not in self._elements:
        self._add_element(triplet, value)
      else:
        self._elements[triplet] = value
  def resize(self, shape):
    self._shape = shape
    old_triplets = self._elements.copy().items()
    self._elements = {}
    self._init_non_empty_z()
    for triplet, value in old_triplets:
      for i in range(len(self._shape)):
        assert(self._shape[i] > triplet[i])
      self._add_element(triplet, value)
  def _init_non_empty_z(self):
    self.non_empty_z = {}
    for i in range(self._shape[0]):
      for j in range(self._shape[1]):
        self.non_empty_z[i, j] = []
  def _delete_element(self, triplet):
    """ Deletes an element. Element is assumed to exist. """
    del self._elements[triplet]
    self.non_empty_z[triplet[0], triplet[1]].remove(triplet[2])
  def _add_element(self, triplet, value):
    """ Adds an element. triplet is assumed to be default value before call. """
    self._elements[triplet] = value
    self.non_empty_z[triplet[0], triplet[1]].append(triplet[2])
  def delete_by_axis(self, index, axis):
    """ Slow deletion of entire row / column / depth """
    old_triplets = self._elements.copy().items()
    self._elements = {}
    self._init_non_empty_z()
    for triplet, value in old_triplets:
      assert(value != self._default_value)
      for i in range(len(self._shape)):
        assert(self._shape[i] > triplet[i])
      if triplet[axis] < index:
        self._add_element(triplet, value)
      elif triplet[axis] > index:
        new_triplet = list(triplet)
        new_triplet[axis] -= 1
        for i in range(len(self._shape)):
          assert(self._shape[i] > new_triplet[i])
        self._add_element(tuple(new_triplet), value)
  def all_non_default_value(self):
    return list(self._elements.keys())

class NFA():
  def __init__(self, terminals, all_accepting=True):
    assert('.' not in terminals and '@' not in terminals)
    self._num_states = 1
    self.terminals = sorted(terminals)
    self._terminals_including_special = ['@', '.'] + self.terminals
    self._terminal_decsription_cost = utils.bits_required(len(self.terminals))
    self._all_accepting = all_accepting
    self._initial = 0
    self._matrix = Sparse3DMatrix(shape=[100,
                                         len(self._terminals_including_special),
                                         100])
    # Needs to be updated when removing states.
    self._accepting = []
    if self._all_accepting:
      self._accepting.append(0)
    # Needs to be updated when adding or removing states.
    self._transition_counts = [0]
    self._bits_per_transition = []
    self._update_bits_per_transition()

  # Public:
  def translate_words(self, words):
    return [[self._terminals_including_special.index(letter) for letter in word] for word in words]

  def parse(self, input_string):
    n = len(input_string)
    k = self._num_states
    # L numpy matrix where in cell i, j is minimal description length to reach
    # state j before scanning input_string[i], or inf if it's unreachable.
    L = {}
    L[0, self._initial] = 0
    # B is a numpy matrix where in cell i, j, is a triplet:
    # (k, l, code for '@' or '.' or input_string[l])
    # iff the optimal predecessor to state j before scanning input_string[i] is
    # state k before scanning input_string[l].
    B = {}
    to_check = set([0])
    accessible = set()
    for i in range(n):
      accessible.clear()
      # Prescan for epsilon transitions.
      while to_check:
        j = to_check.pop()
        accessible.add(j)
        for epsilon_state in self._matrix.non_empty_z[j, 0]:
          accessible.add(epsilon_state)
          dl = L[i, j] + self._bits_per_transition[j]
          if dl < L.get((i, epsilon_state), inf):
            L[i, epsilon_state] = dl
            B[i, epsilon_state] = (i, j, '@')
            to_check.add(epsilon_state)
      to_check.clear()
      # Now deal with all accessible states.
      for j in accessible:
        # Bits required to move from this state.
        dl = L[i, j] + self._bits_per_transition[j]
        for wildcard_state in self._matrix.non_empty_z[j, 1]:
          costly_dl = dl + self._terminal_decsription_cost
          if costly_dl < L.get((i + 1, wildcard_state), inf):
            to_check.add(wildcard_state)
            L[i + 1, wildcard_state] = costly_dl
            B[i + 1, wildcard_state] = (i, j, '.')
        for next_state in self._matrix.non_empty_z[j, input_string[i]]:
          if dl < L.get((i + 1, next_state), inf):
            to_check.add(next_state)
            L[i + 1, next_state] = dl
            B[i + 1, next_state] = (i, j, self._terminals_including_special[
                                    input_string[i]])
    if not self._all_accepting:
      to_check = list(to_check.intersection(self._accepting))
    if to_check:
      min_index = to_check.pop()
      min_dl = L[n, min_index]
      while to_check:
        candidate = to_check.pop()
        if L.get((n, candidate), inf) < min_dl:
          min_dl = L[n, candidate]
          min_index = candidate
      path = []
      last_state = min_index
      last_triplet = B.get((n, min_index), None)
      while last_triplet:
        path = [(last_triplet[2], last_state)] + path
        last_state = last_triplet[1]
        last_triplet = B.get((last_triplet[0], last_triplet[1]), None)
      path = [('N/A', 0)] + path
      return {'dl': min_dl, 'parse': path}
    else:
      return {'dl': False, 'parse': False}

  def get_neighbor(self):
    nfa_copy = copy.deepcopy(self)
    choice_range = 5 if self._all_accepting else 7
    while True:
      chosen_transformation = random.randrange(choice_range)
      if chosen_transformation == 0:
        message = nfa_copy._replace_random_terminal()
      elif chosen_transformation == 1:
        message = nfa_copy._add_random_state()
      elif chosen_transformation == 2:
        message = nfa_copy._remove_random_state()
      elif chosen_transformation == 3:
        message = nfa_copy._add_random_transition()
      elif chosen_transformation == 4:
        message = nfa_copy._remove_random_transition()
      elif chosen_transformation == 5:
        message = nfa_copy._remove_random_accepting()
      elif chosen_transformation == 6:
        message = nfa_copy._add_random_accepting()
      if message:
        break
    
    return nfa_copy, message

  def get_description_length(self):
    bits_for_one_state = utils.bits_required(self._num_states)
    bits_for_one_terminal = utils.bits_required(
        len(self._terminals_including_special))
    dl = 0
    # declare amount of states with 1s, end with a single 0
    dl += self._num_states + 1
    # declare section amount of accepting states and list their indices
    dl += (len(self._accepting) + 1) * bits_for_one_state
    # transitions, each transition is a triplet of two states and a terminal
    dl += (2 * bits_for_one_state + bits_for_one_terminal) * \
        sum(self._transition_counts)
    return dl

  def to_json(self):
    result = {
        'terminals': self.terminals,
        'transitions': [
          {
            's': t[0], # Source
            't': self._terminals_including_special[t[1]], # Terminal
            'd': t[2] # Destination
          }
          for t in self._get_transitions()],
        'initial': self._initial,
        'accepting': self._accepting
    }
    return json.dumps(result)

  def to_graphviz(self):
    output = 'digraph NFA {\n'
    json_dict = json.loads(self.to_json())
    for transition in json_dict['transitions']:
      output += '\t' + str(transition['s']) + ' -> ' + str(transition['d'])
      output += ' [label="' + transition['t'] + '"];\n'
    for accepting in json_dict['accepting']:
      output += '\t' + str(accepting) + ' [peripheries=2];\n'
    output += '\t' + str(json_dict['initial']) + ' [shape=triangle];\n'
    output += '}'
    print(output)
    return output

  # Utilities:

  def _update_bits_per_transition(self):
    self._bits_per_transition = []
    for i in range(self._num_states):
      cost = utils.bits_required(self._transition_counts[i])
      self._bits_per_transition.append(cost)

  def _add_state(self):
    new_index = self._num_states
    self._num_states += 1
    self._transition_counts.append(0)
    self._update_bits_per_transition()
    if self._all_accepting:
      self._accepting.append(new_index)
    return new_index

  def _add_transition(self, origin, destination, terminal):
    assert(not self._matrix[origin, terminal, destination])
    self._matrix[origin, terminal, destination] = True
    self._transition_counts[origin] += 1
    self._update_bits_per_transition()

  def _get_random_terminal(self):
    return random.randrange(len(self._terminals_including_special))

  def _remove_state(self, index):
     # can't delete the inital state.
    assert(index > 0 and index < self._num_states)
    self._num_states -= 1
    new_accepting = []
    for i in self._accepting:
      if i > index:
        new_accepting.append(i - 1)
      if i < index:
        new_accepting.append(i)
    self._accepting = new_accepting
    del self._transition_counts[index]
    self._update_bits_per_transition()
    self._matrix.delete_by_axis(index, 0)
    self._matrix.delete_by_axis(index, 2)

  def _get_transitions(self):
    return self._matrix.all_non_default_value()

  def _remove_transition(self, origin, terminal, destination):
    # Make sure transition exists
    assert(self._matrix[origin, terminal, destination])
    self._transition_counts[origin] -= 1
    self._update_bits_per_transition()
    self._matrix[origin, terminal, destination] = False

  # NFA mutations:
  def _add_random_state(self):
    "Adds a state with one transition going in and one going out."
    i = random.randrange(self._num_states)
    j = random.randrange(self._num_states)
    state_new = self._add_state()
    terminal_in = self._get_random_terminal()
    terminal_out = self._get_random_terminal()
    self._add_transition(i, state_new, terminal_in)
    self._add_transition(state_new, j, terminal_out)
    return 'Added state {}, with {} in from {} and {} out to {}'.format(
        state_new, self._terminals_including_special[terminal_in], i,
        self._terminals_including_special[terminal_out], j)

  def _remove_random_state(self):
    if self._num_states < 2:
      return False
    state_to_remove = random.randrange(1, self._num_states)
    self._remove_state(state_to_remove)
    return 'Removed state {}'.format(state_to_remove)

  def _add_random_transition(self):
    i = random.randrange(self._num_states)
    terminal = self._get_random_terminal()
    j = random.randrange(self._num_states)
    if self._matrix[i, terminal, j]:
      return False
    else:
      self._add_transition(i, j, terminal)
      return 'Added transition {} - {} -> {}'.format(
          i, self._terminals_including_special[terminal], j)

  def _remove_random_transition(self):
    transitions = self._get_transitions()
    if self._num_states < 2 or len(transitions) < 1:
      return False
    i, terminal, j = random.choice(transitions)
    self._remove_transition(i, terminal, j)
    return 'Removed transition {} - {} -> {}'.format(
        i, self._terminals_including_special[terminal], j)

  def _replace_random_terminal(self):
    if sum(self._transition_counts) < 1:
      return False
    i, terminal, j = random.choice(self._get_transitions())
    # Pick a new terminal.
    new_terminal = random.randrange(len(self._terminals_including_special) - 1)
    if new_terminal == terminal:
      new_terminal = len(self._terminals_including_special) - 1
    # Add new transition.
    if self._matrix[i, new_terminal, j]:
      return False
    # Remove transition.
    self._remove_transition(i, terminal, j)
    self._matrix[i, new_terminal, j]
    return 'Replaced transition {} - {} -> {} to {} - {} -> {}'.format(
        i, self._terminals_including_special[terminal], j, i,
        self._terminals_including_special[new_terminal], j)

  def _add_random_accepting(self):
    assert(not self._all_accepting)
    state = random.randrange(self._num_states)
    if state in self._accepting:
      return False
    self._accepting.append(state)
    return 'Added {} to accepting states'.format(state)

  def _remove_random_accepting(self):
    state = random.choice(self._accepting)
    if state == self._initial:
      return False
    self._accepting.remove(state)
    return 'Removed {} from accepting states.'.format(state)


def from_json(json_str_or_obj, all_accepting=True):
  if isinstance(json_str_or_obj, str):
    json_obj = json.loads(json_str_or_obj)
  else:
    json_obj = json_str_or_obj
  nfa = NFA(json_obj['terminals'], all_accepting)
  name_mapping = {}
  name_mapping[json_obj['initial']] = nfa._initial
  names = set([json_obj['initial']])
  for t in json_obj['transitions']:
    names.add(t['s'])
    names.add(t['d'])
  for name in names:
    if name not in name_mapping:
      s = nfa._add_state()
      name_mapping[name] = s
  for t in json_obj['transitions']:
    nfa._add_transition(name_mapping[t['s']],
                        name_mapping[t['d']],
                        nfa._terminals_including_special.index(t['t']))
  for a in json_obj['accepting']:
    if a not in name_mapping:
      s = nfa._add_state()
      name_mapping[a] = s
    if not nfa._all_accepting:
      nfa._accepting.append(name_mapping[a])
  return nfa

def create_initial_nfa(terminals, type_chosen='TERMINAL_STATES'):
  nfa = NFA(terminals)
  if type_chosen == 'ONE_STATE':
    for t in terminals:
      nfa._add_transition(nfa._initial, nfa._initial,
                          nfa._terminals_including_special.index(t))
  if type_chosen == 'TERMINAL_STATES':
    for t in terminals[1:]:
      new_state = nfa._add_state()
    for s in range(nfa._num_states):
      for i in range(len(terminals)):
        nfa._add_transition(
            s, i, nfa._terminals_including_special.index(terminals[i]))
  if type_chosen == 'PAIRS_STATES':
    for t1 in terminals:
      for t2 in terminals:
        new_state = nfa.add_state()
        nfa._add_transition(new_state, new_state,
                            nfa._terminals_including_special.index(t1))
        nfa._add_transition(new_state, initial_state,
                            nfa._terminals_including_special.index(t2))
  return nfa

def test_parsing():
  def tester(word, expected, machine):
    print('checking word: {}'.format(word))
    word = machine.translate_words([word])[0]
    print('result is: {}, expected is {}'.format(machine.parse(word), expected))
  # Test regular parsing
  machine = from_json("""
    {
      "initial": "0",
      "transitions": [{"s": "0", "t": "a", "d":  "0"},
                      {"s": "0", "t": "b", "d":  "1"},
                      {"s": "1", "t": "a", "d":  "0"},
                      {"s": "1", "t": "b", "d":  "1"}],
      "terminals": ["a", "b", "c"],
      "accepting": ["0"]
    }""", False)
  tester('ac', False, machine)
  tester('abababa', 7, machine)
  tester('ababab', False, machine)
  tester('abcabcab', False, machine)
  tester('a', 1, machine)
  tester('', 0, machine)
  tester('b', False, machine)
  tester('c', False, machine)
  tester('ab', False, machine)
  tester('aa', 2, machine)
  # Test epsilon transitions
  machine = from_json("""
    {
      "initial": "0",
      "transitions": [{"s": "0", "t": "@", "d":  "0"},
                      {"s": "0", "t": "@", "d":  "1"},
                      {"s": "1", "t": "@", "d":  "2"},
                      {"s": "2", "t": "a", "d":  "2"}],
      "terminals": ["a", "b", "c"],
      "accepting": ["2"]
    }""", False)
  tester('a', 1, machine)

def test_matrix():
  n = 30
  mat = Sparse3DMatrix(shape=[n, n, n])
  for _ in range(1000000):
    # Insertion
    ind1 = random.randrange(n)
    ind2 = random.randrange(n)
    ind3 = random.randrange(n)
    mat[ind1, ind2, ind3] = random.choice([True, False])

    # Retrieval
    ind1 = random.randrange(n)
    ind2 = random.randrange(n)
    ind3 = random.randrange(n)

    # Retrieval using non_empty_z
    ind1 = random.randrange(n)
    ind2 = random.randrange(n)
    for x in mat.non_empty_z[ind1, ind2]:
      mat.non_empty_z[x, random.randrange(n)]

    # Removal by index and axis
    if random.random() < 0.005:
      indices = [random.randrange(n),
                 random.randrange(n),
                 random.randrange(n)]
      c = random.choice(range(3))
      mat.delete_by_axis(indices[c], c)
