def start_voynich_exp(c):
	voynich = open('data/voynich/FSG.txt').readlines()
	text = ''
	for line in voynich:
	    if line.startswith('#') or not line.strip():
	        continue
	    text += line
	import re
	text = re.sub('\n', '', text)
	text = re.sub('\(.*\)', '', text)
	text = re.sub(',,', ',', text)
	text = re.sub(' ', '', text)
	text = re.sub('-=', '=', text)
	text = re.sub('--', '-', text)
	text = re.sub('_', '', text)

	# Replace line ends with commas
	text = re.sub('-', '', text)
	# Replace paragraph end with commas
	text = re.sub('=', ',', text)
	# Split to words
	words = text.split(',')

	import exp
	import nfa
	import imp
	imp.reload(exp)
	experiment = exp.Experiment('voynich')
	experiment.start(words=words, c=c, iterations=5000, cfg_or_nfa=False, fraction=1)