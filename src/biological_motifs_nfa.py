# This file evaluates an nfa with basic biological motifs.
import exp
import nfa
import utils

sequences = utils.read_fasta('data/human.fasta')
sequences = list(sequences.values())
sequences = [s for s in sequences if not any(c in s for c in ['X', 'B', 'U', 'Z'])]

amino_acids = ['L', 'T', 'R', 'K', 'F', 'W', 'M', 'E', 'G', 'Q', 'D', 'C', 'I', 'P', 'H', 'S', 'A', 'V', 'N', 'Y']
alpha_helix_amino_acids = ['M', 'A', 'L', 'E', 'K']

motif_nfa = nfa.NFA(amino_acids)
alpha_helix = motif_nfa.add_state()
states = [motif_nfa.get_initial()]
for t in amino_acids[1:]:
  new_state = motif_nfa.add_state()
  if t in alpha_helix_amino_acids:
    new_state.add_transition('@', alpha_helix)
  states.append(new_state)
for s in states:
  motif_nfa.add_accepting(s)
  for i in range(len(amino_acids)):
    s.add_transition(amino_acids[i], states[i])

# enter
motif_nfa.get_initial().add_transition('@', alpha_helix)
# stay
for aa in alpha_helix_amino_acids:
  alpha_helix.add_transition(aa, alpha_helix)
# exit
alpha_helix.add_transition('@', motif_nfa.get_initial())
alpha_helix.add_transition('.', alpha_helix)

exp.evaluate(motif_nfa, sequences, fraction=100)