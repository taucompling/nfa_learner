import analysis
import classifiers

import pandas

import numpy

mutation_id = ['Protein ID', 'SIFTPosition', 'SIFTOld', 'SIFTNew']

def get_features(path, required_features):
  data = pandas.read_csv(usecols=required_features + ['Protein ID', 'Substitution'],
                         filepath_or_buffer=path, delimiter='\t')
  new_cols = data.Substitution.str.extract('(?P<SIFTOld>[A-Z])(?P<SIFTPosition>\d+)(?P<SIFTNew>[A-Z])')
  new_cols.SIFTPosition = new_cols.SIFTPosition.astype(int)
  data = pandas.concat([data['Protein ID'], new_cols.SIFTPosition,
                        new_cols.SIFTOld, new_cols.SIFTNew, data[required_features]],
                       axis=1)
  return data.drop_duplicates(subset=['Protein ID', 'SIFTPosition', 'SIFTOld', 'SIFTNew'])