import collections
import math
import multiprocessing
import random
import signal
import sys
import time

import alphabets
import nfa
import cfg
import utils

def subprocess_init(machine_json, cfg_or_nfa):
  signal.signal(signal.SIGINT, signal.SIG_IGN)
  global machine
  if cfg_or_nfa:
    machine = cfg.from_json(machine_json)
  else:
    machine = nfa.from_json(machine_json)

def parse_task(args):
  args[1].put(1)
  return machine.parse(args[0])

class EnergyComputer:
  def __init__(self, terminals, cfg_or_nfa):
    self.trivial_dl_per_symbol = utils.bits_required(len(terminals))
    self.cfg_or_nfa = cfg_or_nfa
  def compute(self, machine, words, progress_checks=False, debug=False):
    if debug:
      with utils.Profiler():
        results = []
        for word in words:
          print(len(word))
          results.append(machine.parse(word))
    else:
      q = multiprocessing.Manager().Queue()
      p = multiprocessing.Pool(initializer=subprocess_init,
                               initargs=[machine.to_json(), self.cfg_or_nfa])
      results = p.map_async(parse_task, [[word, q] for word in words])
      # Check progress
      while not results.ready() and progress_checks:  
        print(q.qsize())
        time.sleep(1)
      results = results.get()
      p.close()
    return results
  def compute_with_penalties(self, machine, words, debug=False):
    dls = [result['dl'] for result in self.compute(machine, words, debug=debug)]
    partial_parse_ability = False
    for i in range(len(dls)):
      if not dls[i]:
        dls[i] = len(words[i]) * self.trivial_dl_per_symbol * 2
        partial_parse_ability = True
    penalty = 0
    if partial_parse_ability:
      penalty += len(words)
    return sum(dls) + machine.get_description_length() + penalty

class Experiment:
  def __init__(self, filename='debug'):
    self.logger = utils.Logger('output/' + filename)
  def decide(self, old_energy, new_energy, temperature):
    """ Returns true if decided to move to new_energy """
    delta = new_energy - old_energy
    self.logger.log('Energy delta is {:.4%}'.format(delta / self.trivial_energy))
    if delta < 0:
      return True
    probability = math.exp(- delta / temperature)
    self.logger.log('Probability to move to neighbor: {:.2%}'.format(probability))
    return random.random() < probability
  def start(self, words, c=500, iterations=5000, cfg_or_nfa=False, machine=None,
            fraction=1):
    terminals = set()
    # A sort of validation that make sure we have expected terminals
    for word in words:
      terminals |= set(word)
    terminals = list(terminals)
    self.logger.log('Set of terminals ({}): {}'.format(len(terminals),
                                                       terminals))
    if cfg_or_nfa:
      if not machine:
        machine = cfg.create_initial_cfg(terminals=terminals)
    else:
      if not machine:
        machine = nfa.create_initial_nfa(terminals=terminals)
        words = machine.translate_words(words)
    energy_computer = EnergyComputer(terminals, cfg_or_nfa)
    self.trivial_energy = sum(len(word) for word in words) * utils.bits_required(len(terminals))
    self.logger.log('Trivial energy is {:.2f}.'.format(self.trivial_energy))
    # start the annealing process
    old_energy = self.trivial_energy
    iteration = 1
    scores = collections.deque(maxlen=50)
    while True:
      temperature = c / math.log(1 + iteration/1000)
      # Check if we've reached experiment end
      if iteration > iterations:
        try:
          iterations += int(input('Iterations ended. Keep going? '
                                  '(enter integer for iterations or any other key to end)\n'))
          print('Continuing.')
          continue
        except:
          print('Ending.')
          break
      self.logger.log()
      # Experiment iteration
      with utils.Timer():
        self.logger.log('Iteration {}/{} with temperature {:.2f}'.format(iteration, iterations, temperature))
        neighbor, message = machine.get_neighbor()
        self.logger.log(message, utils.Logger.OKBLUE)
        if fraction > 1:
          sample = utils.sample_fraction(words, fraction)
          old_energy = energy_computer.compute_with_penalties(machine, sample)
          scores.append(fraction * old_energy / self.trivial_energy)
          
          self.logger.log('Current energy per moving average: '
                          '{:.2%}'.format(sum(scores) / len(scores)))
        else:
          sample = words
        new_energy = energy_computer.compute_with_penalties(neighbor, sample)
        if self.decide(old_energy, new_energy, temperature):
          machine = neighbor
          old_energy = new_energy
          self.logger.log('Move to neighbor with energy {:.2%} '
                          'of which {:.4%} is used for the machine description.'.format(fraction * new_energy / self.trivial_energy,
                                                                                        machine.get_description_length() / self.trivial_energy),
                          utils.Logger.OKGREEN)
          self.logger.log(machine.to_json())
        iteration += 1
  def start_with_data(self, machine_dict=None, cfg_or_nfa=False,
                      iterations=10000, c=300, fraction=1,
                      path='data/SCOP_amino_acid.fasta', filter_by=None,
                      is_mla18ss=False, reduce_alphabet=False):
    self.logger.log('Path for data: {}\n'.format(path))
    machine = None
    if machine_dict:
      import json
      json_encoded = json.dumps(machine_dict)
      if cfg_or_nfa:
        machine = cfg.from_json(json_encoded)
      else:
        machine = nfa.from_json(json_encoded)
    sequences = utils.read_fasta(path)
    
    if filter_by:
      sequences = utils.filter_sequences(sequences, filter_by)
    
    if is_mla18ss:
      sequences = alphabets.process_mla18ss(sequences, True)
    else:
      sequences = alphabets.process_amino_acids(sequences, reduce_alphabet)
    self.start(words=list(sequences.values()), machine=machine,
               iterations=iterations, c=c, fraction=fraction,
               cfg_or_nfa=cfg_or_nfa)

def combine_and_evaluate_nfa(a, b, words):
  import json
  c = nfa.from_json(json.dumps(a)) | nfa.from_json(json.dumps(b))
  evaluate(machine=c, words=words, cfg_or_nfa=False)

def evaluate(machine, words, fraction=1, cfg_or_nfa=False):
  sampling = utils.sample_fraction(words, fraction)
  sampling = machine.translate_words(sampling)
  sum = 0
  for s in sampling:
    sum += len(s)
  trivial_energy = sum * utils.bits_required(len(machine.terminals))
  assert(machine.terminals)
  energy_computer = EnergyComputer(machine.terminals, cfg_or_nfa)
  print('{:.2%}'.format(energy_computer.compute_with_penalties(machine, sampling) / trivial_energy))
