from sklearn import linear_model
from sklearn import datasets as sklearn_data
from sklearn import tree
from sklearn.ensemble import AdaBoostClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.neural_network import BernoulliRBM
from sklearn.pipeline import Pipeline

import numpy

custom_classifier_threshold = 0.6
class MyClassifier():
  def __init__(self, cols):
    self.cols = cols
  def fit(self, train_X, train_y):
    self.models = []
    for columns in self.cols:
      model = DecisionTreeClassifier(max_depth=max_depth)
      model.fit(train_X[:, columns], train_y)
      self.models.append(model)
  def find_disagreements(self, test_X):
    results = []
    for i in range(len(self.models)):
      results.append(self.models[i].predict_proba(test_X[:, self.cols[i]])[:, 1])
    return numpy.abs(results[0] - results[1]) > custom_classifier_threshold
  def predict_proba(self, test_X):
    results = []
    for i in range(len(self.models)):
      results.append(self.models[i].predict_proba(test_X[:, self.cols[i]])[:, 1])
    disputed_indices = numpy.abs(results[0] - results[1]) > custom_classifier_threshold
    results[-1][disputed_indices] = results[2][disputed_indices]
    return results[-1]


max_depth = 10

def get_classifier(train_X, train_y):
  model = RandomForestClassifier(max_depth=max_depth)
  # model = SVC(probability=True)
  model.fit(train_X, train_y)
  return model

def classify(train_X, train_y, test_X, algo):
  if algo == 1: # Logistic regression
    model = linear_model.LogisticRegression()
  elif algo == 2: # Linear regression
    assert(False) # TDOO: sort out implementation
    model = linear_model.LinearRegression()
    model.fit(train_X, train_y)
    probs = model.predict(test_X)
  elif algo == 3: # Decision tree
    model = DecisionTreeClassifier(max_depth=max_depth)
    # tree.export_graphviz(m, out_file='tree.dot')
  elif algo == 4: # Random forest
    model = RandomForestClassifier(max_depth=max_depth)
  elif algo == 5: # AdaBoost
    model = AdaBoostClassifier()
  elif algo == 6: # SVM
    model = SVC(probability=True)
  elif algo == 7: # Custom classifier
    assert(False) # TDOO: sort out implementation
    cols = [
      [2, 3, 4, 5, 6, 7], # polyphen non-homology features
      [8, 9, 10, 11, 12], # polyphen homology features
      [0, 1] + list(range(13, train_X.shape[1])), # inducer features
      list(range(train_X.shape[1])) # all
    ]
    model = MyClassifier(cols)
    model.fit(train_X, train_y)
    return model.predict_proba(test_X)
  elif algo == 8: # RBM + logistic
    logistic = linear_model.LogisticRegression()
    rbm = BernoulliRBM(random_state=0, verbose=True)
    model = Pipeline(steps=[('rbm', rbm), ('logistic', logistic)])
    #rbm.learning_rate = 0.06
    #rbm.n_iter = 20
    #rbm.n_components = 100
    #logistic.C = 6000.0
  model.fit(train_X, train_y)
  y_probabilities = model.predict_proba(test_X)
  probs = y_probabilities[:, 1]
  return probs
