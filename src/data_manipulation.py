# This section turns mutation files into fasta format.

import utils
import pandas

# Given csv's of mutations, returns involved proteins.
def get_all_participating_proteins(csvs):
  proteins = set()
  for csv in csvs:
    proteins |= set(pandas.read_csv(csv)['Protein'].unique())
  print('unique proteins in total: {}'.format(len(proteins)))
  return proteins


mutation_tsv = ['data/humvar/humvar.neutral.tsv', 'data/humvar/humvar.deleterious.tsv',
                'data/humsavar/humsavar.neutral.tsv', 'data/humsavar/humsavar.deleterious.tsv',
                'data/humdiv/humdiv.neutral.tsv', 'data/humdiv/humdiv.deleterious.tsv',
                'data/PMC3665581/deleterious.tsv']

# Given original sequence file, transforms csv of protein,position,old,new to fasta of mutation
def mutation_file_to_fasta(files=mutation_tsv, output_filename='data/mutation.aa.fasta',
                           original_sequences='data/original.aa.fasta', clean_names=True):
  proteins = utils.read_fasta(original_sequences, clean_names)
  mutated_proteins = {}
  total_ignored = 0
  for file in files:
    for _, row in pandas.read_csv(file).iterrows():
      if row['Protein'] not in proteins:
        print('Missing sequence for:', row)
        total_ignored += 1
        continue
      sequence = proteins[row['Protein']]
      position = int(row['Position']) - 1
      old = row['Old'].upper()
      new = row['New'].upper()
      if not position < len(sequence):
        print('Ignoring sequence because of position longer than sequence',
              str(row))
        total_ignored += 1
        continue
      if old != sequence[position]:
        print('Ignore sequence because of mismatch:', row)
        total_ignored += 1
        continue
      sequence = sequence[0:position] + new + sequence[position+1:]
      key = ','.join([row['Protein'], str(row['Position']), old, new])
      if key in mutated_proteins:
        assert(sequence == mutated_proteins[key])
      else:
        mutated_proteins[key] = sequence
  print('Ignored in total:', total_ignored)
  utils.write_fasta(output_filename, mutated_proteins)

import alphabets
import utils

def produce_mla18ss(aa_file, ss_file, output):
  mla18ss_table = [['A', 'F', 'R', 'K', 'I', 'S'],
                   ['M', 'P', 'N', 'D', 'Q', 'T'],
                   ['C', 'G', 'H', 'E', 'L', 'W']]
  mla18ss = {}
  skipped = 0

  aa = utils.read_fasta(aa_file)
  ss = utils.read_fasta(ss_file)

  def translate_to_mla18ss(aa, ss):
    aa_index = None
    ss_index = None

    if aa in ['R', 'K']:
      aa_index = 0
    elif aa in ['D', 'E']:
      aa_index = 1
    elif aa in ['I', 'L', 'M', 'A', 'V']:
      aa_index = 2
    elif aa in ['F', 'Y', 'W']:
      aa_index = 3
    elif aa in ['N', 'Q', 'H', 'S', 'T']:
      aa_index = 4
    elif aa in ['C', 'P', 'G']:
      aa_index = 5
    else:
      print('encountered weird amino acid: ' + aa)
      assert(False)

    if ss == 'H':
      ss_index = 0
    elif ss == 'E':
      ss_index = 1
    elif ss == 'C':
      ss_index = 2
    else:
      print('encountered weird structural annotation: ' + ss)
      assert(False)

    return mla18ss_table[ss_index][aa_index]

  for k in aa.keys():
    aa_seq = aa[k]
    if k in ss:
      ss_seq = ss[k]
    else:
      print('No structural data for record {}'.format(k))
      skipped += 1
      continue
    assert(len(aa_seq) == len(ss_seq))
    try:
      mla18ss[k] = ''.join([translate_to_mla18ss(aa_seq[i], ss_seq[i]) for i in range(len(ss_seq))])
    except:
      skipped += 1

  print('Overall skipped ' + str(skipped))
  utils.write_fasta(output, mla18ss)



# This section filters the mutated sequences - it leaves only mutations that
# caused a difference under the mla18ss alphabet

def dedup(filename, original_sequences):
  fasta = utils.read_fasta(filename)
  keys_to_remove = []
  processed_fasta = alphabets.process_mla18ss(fasta)
  c = 0
  for k in processed_fasta:
    protein_name, _, _, _ = k.split('\t')
    if processed_fasta[k] == original_sequences[protein_name]:
      c += 1
      keys_to_remove.append(k)
  print('Synonymous: {:.2%} ({})'.format(c / len(processed_fasta), filename))
  for k in keys_to_remove:
    del fasta[k]
  utils.write_fasta(filename + '.non_syn', fasta)

def dedup_all_files(original_sequences, filenames):
  original_sequences = utils.read_fasta(original_sequences)
  original_sequences = alphabets.process_mla18ss(original_sequences)
  for filename in filenames:
    dedup(filename, original_sequences)
