# Tools for analysing the parses produced by the NFA


# Gets the first index where the parses different, and the reversed first index.
def diff(parse1, parse2):
  i = 0
  while parse1[i] == parse2[i]:
    i += 1

  j = -1
  while parse1[j] == parse2[j]:
    j -= 1

  print(i, j)
  return parse1[i:j+1], parse2[i:j+1]

# Computes the histogarm of states in a parse
def compute_histogram(parse):
  histogram = {}
  for pair in parse[1:]:
    if pair[1] in histogram:
      histogram[pair[1]] += 1
    else:
      histogram[pair[1]] = 1
  return histogram

# n_states needs to be provided because some states may not appear in the
# parse but we still want to get 0 for them.
def per_state_histogram_delta(parse1, parse2, n_states):
  histogram1 = compute_histogram(parse1)
  histogram2 = compute_histogram(parse2)
  return [histogram1.get(k, 0) - histogram2.get(k, 0) for k in range(n_states)]

# Sum delta of state histogram columns
def histogram_delta(parse1, parse2):
  histogram1 = compute_histogram(parse1)
  histogram2 = compute_histogram(parse2)
  s = 0
  for k in histogram1:
    s += abs(histogram1[k] - histogram2.get(k, 0))
  return s

# Array distance.
def LD(s,t):
  d = {}
  S = len(s)
  T = len(t)
  for i in range(S):
    d[i, 0] = i
  for j in range (T):
    d[0, j] = j
  for j in range(1,T):
    for i in range(1,S):
      if s[i] == t[j]:
        d[i, j] = d[i-1, j-1]
      else:
        d[i, j] = min(d[i-1, j] + 1, d[i, j-1] + 1, d[i-1, j-1] + 1)
  return d[S-1, T-1]
