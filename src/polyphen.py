import analysis
import classifiers

from sklearn.cross_validation import KFold
from sklearn.preprocessing import Imputer
import numpy
import pandas
import random

directory = 'data/humvar_and_humdiv/polyphen/'

suffix = '.tsv'

mutation_id = ['o_acc', 'o_pos', 'o_aa1', 'o_aa2']

paths = [[directory + 'humdiv_neutral' + suffix,
          directory + 'humdiv_deleterious' + suffix],
         [directory + 'humvar_neutral' + suffix ,
          directory + 'humvar_deleterious' + suffix]]

features = {}
features['final'] = ['pph2_prob']
features['non-homo'] = ['dVol', 'PfamHit', 'CpG', 'NormASA', 'B-fact', 'dProp']
features['homo'] = ['Score1', 'Score2', 'dScore', 'Nobs', 'IdPmax', 'IdQmin']
features['all'] =  features['non-homo'] + features['homo']
features['test'] = ['Nobs']

def find_unsure():
  unsure_proteins = set()
  unsure_mutations = set()
  fields = ['o_acc','o_pos', 'o_aa1', 'o_aa2']
  for db in paths:
    neutral = pandas.read_csv(filepath_or_buffer=db[0], na_values='?',
    													delimiter='\t')
    deleterious = pandas.read_csv(filepath_or_buffer=db[1], na_values='?',
    														  delimiter='\t')

    # By prediction
    # unsure_deleterious = deleterious[deleterious.prediction == 'possibly damaging']
    # unsure_neutral = neutral[neutral.prediction == 'possibly damaging']
    # By pph2_prob
    unsure_deleterious = deleterious[(0.4 < deleterious.pph2_prob) & (deleterious.pph2_prob < 0.6)]
    unsure_neutral = neutral[(0.4 < neutral.pph2_prob) & (neutral.pph2_prob < 0.6)]

    def add_unsure(x):
      unsure_mutations.add((x.o_acc, x.o_pos, x.o_aa1, x.o_aa2))
      unsure_proteins.add(x.o_acc)

    for unsure in [unsure_neutral, unsure_deleterious]:
      unsure.apply(axis=1, func=add_unsure)
  return unsure_mutations

def find_mistakes():
  mistakes_mutations = set()
  mistakes_proteins = set()
  fields = ['o_acc','o_pos', 'o_aa1', 'o_aa2']
  for db in paths:
    neutral = pandas.read_csv(filepath_or_buffer=db[0], na_values='?',
    													delimiter='\t')
    deleterious = pandas.read_csv(filepath_or_buffer=db[1], na_values='?',
    															delimiter='\t')
    
    #print(db[0].split('/')[-1])
    false_negatives = neutral[neutral.prediction != 'benign']
    #print(len(false_negatives), '/', len(neutral))
    
    #print(db[1].split('/')[-1])
    false_positives = deleterious[deleterious.prediction == 'benign']
    #print(len(false_positives), '/', len(deleterious))
    

    def add_mistake(x):
      mistakes_mutations.add((x.o_acc, x.o_pos, x.o_aa1, x.o_aa2))
      mistakes_proteins.add(x.o_acc)

    for mistakes in [false_negatives, false_positives]:
      mistakes.apply(axis=1, func=add_mistake)
  return mistakes_mutations

def find_disagreements():
  disagreement_mutations = set()
  for i in range(2):
    per_protein_samples = analysis._get_all_features_per_protein(
      filenames=analysis.datasets[i], mutation_features=False,
      provean_features=[], nfa_features=[], polyphen_features=features['all'])
    protein_names = []
    for k, _ in per_protein_samples:
      protein_names.append(k)
    random.shuffle(protein_names)
    # 10 fold cross validation
    for train_ind, test_ind in KFold(len(protein_names), n_folds=10):
      train_protein_names = [protein_names[i] for i in train_ind]
      train = pandas.concat([per_protein_samples.get_group(name) for name in train_protein_names]).values[:, 4:]
      test_protein_names = [protein_names[i] for i in test_ind]
      test = pandas.concat([per_protein_samples.get_group(name) for name in test_protein_names]).values[:, 4:]
      train_X = train[:, :-1].astype(numpy.float64)
      train_y = train[:, -1].astype('int')
      test_X = test[:, :-1].astype(numpy.float64)
      test_y = test[:, -1].astype('int')
      # fit a model
      imp = Imputer(missing_values='NaN', strategy='mean', axis=0)
      train_X = imp.fit_transform(train_X)
      test_X = imp.fit_transform(test_X)
      model = classifiers.MyClassifier(
      	[[0, 1, 2, 3, 4, 5], # polyphen non-homology features
				 [6, 7, 8, 9, 10]]) # polyphen homology features)
      model.fit(train_X, train_y)
      # test disagreements
      disagreements = model.find_disagreements(test_X)
      for x in pandas.concat([per_protein_samples.get_group(name) for name in train_protein_names]).values[:, :4][disagreements]:
        disagreement_mutations.add((x[0], x[1], x[2], x[3]))
    return disagreement_mutations

def get_features(path, required_features):
  pp_csv = pandas.read_csv(usecols=required_features + mutation_id,
                           filepath_or_buffer=path, na_values='?',
                           delimiter='\t')
  def func(val):
    if isinstance(val, str):
      if val == 'Yes' or val.startswith('PF'):
        val = 1
      if val == 'No' or val == 'NO':
        val = 0
    return val
  return pp_csv.applymap(func)
