import gensim
import utils

def make_model(name='aa_embeddings', fasta='data/SCOP/SCOP.aa.fasta'):
  sentences = list(utils.read_fasta(fasta).values())
  model = gensim.models.word2vec.Word2Vec(sentences=sentences,
                                          workers=4, size=4, window=100)
  model.save(name)

def load_model(filename):
  model = gensim.models.word2vec.Word2Vec.load(filename)
  for aa in model.vocab:
    print(aa, model[aa])

def plot_model(filename='embeddings.aa'):
  model = gensim.models.word2vec.Word2Vec.load(filename)
  disallowed = ['X', '-']
  labels = [aa for aa in model.vocab if (str(aa) not in disallowed)]
  X = [model[aa] for aa in model.vocab if (str(aa) not in disallowed)]
  import matplotlib.pyplot as plt
  fig = plt.figure()
  from sklearn import decomposition
  pca = decomposition.PCA(n_components=2)
  pca.fit(X)
  X = pca.transform(X)
  print(X, labels)
  plt.plot(X[:, 0], X[:, 1], 'ro')
  for label, x, y in zip(labels, X[:, 0], X[:, 1]):
    plt.annotate(label, xy=(x, y))
  plt.savefig(filename + '.w2v.png')
  plt.close()
