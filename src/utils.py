import datetime
import math
import random
import sys
import time
import threading
import re


# UniProt stuff

import urllib
def translate_id_(query_ids, from_id, to_id):
  url = 'http://www.uniprot.org/mapping/'

  params = {
    'from': from_id,
    'to': to_id,
    'format': 'tab',
    'query': ' '.join(query_ids)
  }

  data = urllib.parse.urlencode(params)
  response = urllib.request.urlopen(url, data.encode('utf-8')).read().decode('utf-8')
  print(response)
  lines = response.split('\n')
  lines = [line for line in lines[1:] if line]
  result = {}
  for line in lines:
    ids = line.split('\t')
    result[ids[0]] = ids[1]
  return result

def refseq_to_uniprot(query_ids):
  return translate_id_(query_ids, 'P_REFSEQ_AC', 'ID')

def geneid_to_uniprot(query_ids):
  return translate_id_(query_ids, 'P_ENTREZGENEID', 'ID')

def get_sequence(uniprot_id):
  url = 'http://www.uniprot.org/uniprot/' + uniprot_id + '.fasta'
  response = urllib.request.urlopen(url).read().decode('utf-8')
  lines = response.split('\n')
  return ''.join(lines[1:])

# plot

import matplotlib as mpl
# agg backend is used to create plot as a .png file
mpl.use('agg')
import matplotlib.pyplot as plt
def boxplot(data_to_plot, filename='fig1.png'):
  # Create a figure instance
  fig = plt.figure()
  # Create an axes instance
  ax = fig.add_subplot(111)
  # Create the boxplot
  bp = ax.boxplot(data_to_plot)
  # Save the figure
  fig.savefig(filename, bbox_inches='tight')

def get_keys_values(d):
  keys = list(d.keys())
  values = []
  for k in keys:
    values.append(d[k])
  return keys, values

def avg(l):
  return sum(l) / len(l)

def sample_fraction(l, fraction):
  return random.sample(l, int(len(l) / fraction))

def transcript(words, mapping):
  table = str.maketrans(mapping)
  return [word.translate(table) for word in words]

def filter_sequences(sequences, filter_by):
  filtered_sequences = {}
  for k in sequences:
    if filter_by in k:
      filtered_sequences[k] = sequences[k]
  print('{} sequences after filtering.'.format(len(filtered_sequences)))
  return filtered_sequences

def read_fasta(path, clean_names=False, limit=None):
  def trunc_at(s, b, start ,end):
    return b.join(s.split(b)[start:end])
  protein_sequences = {}
  with open(path, 'r') as f:
    entries = re.split('^>', f.read(), flags=re.MULTILINE)
    bad_sequences = 0
    end = limit if limit else len(entries)
    for entry in entries[1:end]:
      rows = entry.splitlines()
      name = rows[0]
      sequence = ''.join(rows[1:]).upper()
      if (len(sequence) > 0):
        protein_sequences[name] = sequence
      else:
        bad_sequences += 1
    print('{} records in fasta file. {} were ignored due to missing sequences.'.format(len(entries) - 1,
                                                                                       bad_sequences))
    if clean_names:
      return cleanup_names(protein_sequences)
    else:
      return protein_sequences

def cleanup_names(protein_with_junk_titles):
  proteins = {}
  for p in protein_with_junk_titles:
    match = re.match('.*\|(.*)\|.*', p)
    only_protein_name = match.group(1) if match else p
    proteins[only_protein_name] = protein_with_junk_titles[p]
  return proteins

def write_fasta(path, sequences_dict):
  with open(path, 'w+') as f:
    for k in sequences_dict:
      f.write('>' + k + '\n')
      f.write(sequences_dict[k] + '\n')
  print('written fasta to ' + path)

def split_fasta(path, parts, output_path):
  fasta_dict = read_fasta(path=path)
  fasta_keys = list(fasta_dict.keys())
  def chunk_it(seq, parts):
    out = [[] for i in range(parts)]
    for i in range(len(seq)):
      out[i % parts].append(seq[i])
    return out
  chunks = chunk_it(fasta_keys, parts)
  for i in range(len(chunks)):
    write_fasta(output_path + '.' + str(i) + '.fasta',
                {k: fasta_dict[k] for k in chunks[i]})

from collections import Counter
def letter_histogram(words):
  d = Counter(letter for word in words for letter in word)
  for letter in d:
    print('{} | {}'.format(letter, d[letter]))
  return d

# Compute ngrams for given array of words.
def letter_ngrams(words, n=2):
  words = ' '.join(words)
  ngrams = [words[i:i+n] for i in range(len(words)-n+1)]
  ngrams = [x for x in ngrams if ' ' not in x]
  return list(set(ngrams))

class IndexedSet():
  def __init__(self, items=[]):
    self._set = set(items)
    self._list = list(self._set)
    self._map = {v: i for i, v in enumerate(self._list)}
  def __getitem__(self, key):
    return self._list[key]
  def __or__(self, other):
    return IndexedSet(self._set | other._set)
  def __sub__(self, other):
    return IndexedSet(self._set - other._set)
  def __update__(self, other):
    self._set |= other
    self._list = list(self.set)
    self._map = {k: v for k, v in enumerate(self._list)}
  def __getitem__(self, key):
    return self._list[key]
  def __iter__(self):
    return self._list.__iter__()
  def __len__(self):
    return self._list.__len__()
  def __repr__(self):
    return 'IndexedSet(' + str(self._list) + ')'
  def add(self, item):
    self._set.add(item)
    self._list = list(self._set)
    self._map = {v: i for i, v in enumerate(self._list)}
  def remove(self, item):
    self._set.remove(item)
    self._list = list(self._set)
    self._map = {v: i for i, v in enumerate(self._list)}
  def discard(self, item):
    if item in self._set:
      self.remove(item)
  def index(self, item):
    return self._map[item]

def set_choice(set):
  return random.sample(set, 1)[0]

def bits_required(n):
  if (n == 0):
    return 0
  else:
    # return math.ceil(math.log(n, 2))
    return math.log(n, 2)

def now():
  return datetime.datetime.now()

# Prints one of the following formats*:
# 1.58 days
# 2.98 hours
# 9.28 minutes # Not actually added yet, oops.
# 5.60 seconds
# 790 milliseconds
# *Except I prefer abbreviated formats, so I print d,h,m,s, or ms. 
def format_delta(start,end):

  # Time in microseconds
  one_day = 86400000000
  one_hour = 3600000000
  one_second = 1000000
  one_millisecond = 1000

  delta = end - start

  build_time_us = delta.microseconds + delta.seconds * one_second + delta.days * one_day

  days = 0
  while build_time_us > one_day:
    build_time_us -= one_day
    days += 1

  if days > 0:
    time_str = '%.2fd' % ( days + build_time_us / float(one_day) )
  else:
    hours = 0
    while build_time_us > one_hour:
      build_time_us -= one_hour
      hours += 1
    if hours > 0:
      time_str = '%.2fh' % ( hours + build_time_us / float(one_hour) )
    else:
      seconds = 0
      while build_time_us > one_second:
        build_time_us -= one_second
        seconds += 1
      if seconds > 0:
        time_str = '%.2fs' % ( seconds + build_time_us / float(one_second) )
      else:
        ms = 0
        while build_time_us > one_millisecond:
          build_time_us -= one_millisecond
          ms += 1
        time_str = '%.2fms' % ( ms + build_time_us / float(one_millisecond) )
  return time_str

class Timer:
  def __enter__(self):
    self.begin = now()

  def __exit__(self, type, value, traceback):
    print(format_delta(self.begin, now()))

import cProfile, pstats
class Profiler:
  def __enter__(self):
    self.pr = cProfile.Profile()
    self.pr.enable()

  def __exit__(self, type, value, traceback):
    self.pr.disable()
    sortby = 'cumulative'
    ps = pstats.Stats(self.pr).sort_stats(sortby)
    ps.print_stats()

class Logger:
  HEADER = '\033[95m'
  OKBLUE = '\033[94m'
  OKGREEN = '\033[92m'
  WARNING = '\033[93m'
  FAIL = '\033[91m'
  ENDC = '\033[0m'
  def __init__(self, filename=None):
    if not filename:
      filename = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H%M')
    self.terminal = sys.stdout
    self.log_file = open(filename, 'w')

  def log(self, message='', color=None):
    self.log_file.write(message + '\n')
    if color:
      message = color + message + Logger.ENDC
    if len(message) < 200:
      self.terminal.write(message + '\n')
    else:
      self.terminal.write(message[0:200] + '...\n')
    self.flush()
  
  def flush(self):
    self.log_file.flush()
    self.terminal.flush()

def levenshtein_distance(s, t):
  if (s == t):
    return 0
  if len(s) == 0:
    return len(t)
  if len(t) == 0:
    return len(s)
  v0 = range(len(t) + 1)
  for i in range(len(s)):
    v1 = [None]*(len(t) + 1)
    v1[0] = i + 1
    for j in range(len(t)):
      cost = 0 if s[i] == t[j] else 1
      v1[j + 1] = min(v1[j] + 1, v0[j + 1] + 1, v0[j] + cost)
    v0 = v1
  return v1[len(t)]

def merge_fastas(fastas, merged_file):
  result = {}
  for fasta in fastas:
    fasta_dict = read_fasta(fasta)
    for k in fasta_dict:
      if k in result:
        if fasta_dict[k] != result[k]:
          print(k, '\n', fasta_dict[k], '\n', result[k], '\n', fasta)
          assert(False)
      else:
        result[k] = fasta_dict[k]
  write_fasta(merged_file, result)

def chunks(l, n):
  """Yield successive n-sized chunks from l."""
  for i in range(0, len(l), n):
    yield l[i:i+n]
