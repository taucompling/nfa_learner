import utils

amino_acids = ['R', 'K', 'D', 'E', 'Q', 'N', 'H', 'S', 'T', 'Y', 'C', 'M', 'W', 
               'A', 'I', 'L', 'F', 'V', 'P', 'G']

three_to_one = {'ala': 'A', 'arg': 'R', 'asn': 'N', 'asp': 'D', 'asx': 'B',
                'cys': 'C', 'glu': 'E', 'gln': 'Q', 'glx': 'Z', 'gly': 'G',
                'his': 'H', 'ile': 'I', 'leu': 'L', 'lys': 'K', 'met': 'M',
                'phe': 'F', 'pro': 'P', 'ser': 'S', 'thr': 'T', 'trp': 'W',
                'tyr': 'Y', 'val': 'V', 'sec': 'U'}

three_to_one_cap = {'Gly': 'G', 'Met': 'M', 'Gln': 'Q', 'Cys': 'C', 'Leu': 'L',
                    'Arg': 'R', 'Asx': 'B', 'Phe': 'F', 'His': 'H', 'Lys': 'K',
                    'Tyr': 'Y', 'Ser': 'S', 'Thr': 'T', 'Asp': 'D', 'Glx': 'Z',
                    'Trp': 'W', 'Asn': 'N', 'Ala': 'A', 'Glu': 'E', 'Val': 'V',
                    'Ile': 'I', 'Pro': 'P', 'Sec': 'U'}

def process_mla18ss(sequences, allowed_domains_only=False):
  if allowed_domains_only:
    with open('data/SCOP_good_domains_list.txt', 'r') as f:
      allowed = f.read().split('\n')
      sequences = {k: sequences[k] for k in sequences if k in allowed}
  # print('Number of protein sequences after taking only allowed: {}'.format(len(sequences)))
  disallowed_characters = ['-', 'X']
  sequences = filter_sequences(sequences, disallowed_characters)
  return sequences

def process_amino_acids(sequences, reduced_alphabet=True):
  disallowed_characters = ['X', 'B', 'U', 'Z', '-']
  sequences = filter_sequences(sequences, disallowed_characters)
  if reduced_alphabet:
    # remapping into 3 groups
    """alphabet_remapping = {
      # charged
      'R': 'c', 'K': 'c', 'D': 'c', 'E': 'c',
      # polar
      'Q': 'p', 'N': 'p', 'H': 'p', 'S': 'p', 'T': 'p', 'Y': 'p', 'C': 'p', 'M': 'p', 'W': 'p',
      # hydrophobic
      'A': 'h', 'I': 'h', 'L': 'h', 'F': 'h', 'V': 'h', 'P': 'h', 'G': 'h'
    }"""
    # remapping into 12 groups
    alphabet_remapping = {
      'X': 'L', 'L': 'L', 'V': 'L', 'I': 'L', 'M': 'L', # note X gets L as it is most frequent
      'C': 'C',
      'A': 'A',
      'G': 'G',
      'S': 'S', 'T': 'S',
      'P': 'P',
      'F': 'F', 'Y': 'F', 'W': 'F',
      'E': 'E', 'D': 'E', 'N': 'E', 'Q': 'E', 'Z': 'E', # Z signifies either E or Q, E is more common
      'K': 'K', 'R': 'K',
      'H': 'H',
    }
    sequences = reduce_alphabet(sequences, alphabet_remapping)
  return sequences

def reduce_alphabet(sequences, alphabet_remapping):
  return {k: v for k, v in zip(list(sequences.keys()), utils.transcript(list(sequences.values()), alphabet_remapping))}

def filter_sequences(sequences, disallowed_characters):
  return {k: sequences[k] for k in sequences if not any(c in sequences[k] for c in disallowed_characters)}