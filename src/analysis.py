import alphabets
import constants
import classifiers
import exp
import parse_analysis
import polyphen
import pyroc
import provean
import sift
import utils

from sklearn.cross_validation import KFold
from sklearn.preprocessing import Imputer
import csv
import gensim
import numpy
import pandas
import random
import scipy.stats
import sys


def _apply_snp(seq, position, new, old=None):
  if old:
    assert(seq[position] == old)  
  return seq[:position] + new + seq[position + 1:]

def compute_positional_sensitivity(sequence, position,
                                   machine=constants.best_mla18ss):
  assert(position < len(sequence))
  original = machine.parse(machine.translate_words([sequence])[0])
  results = []
  for terminal in machine.terminals:
    mutation = _apply_snp(sequence, position, terminal)
    results.append(machine.parse(machine.translate_words([mutation])[0]))
  return sum([x['dl'] for x in results]) / len(machine.terminals) - original['dl']

def _get_dls(machine, sequences):
  sequences_dict = alphabets.process_mla18ss(sequences)
  bits_per_symbol = utils.bits_required(len(machine.terminals))
  keys, values = utils.get_keys_values(sequences_dict)
  results = exp.EnergyComputer(machine.terminals, False).compute(
      machine, machine.translate_words(values), True)
  dls = [None]*len(results)
  normalized_dls = [None]*len(results)
  parses = [None]*len(results)
  for i in range(len(results)):
    trivial = len(values[i]) * bits_per_symbol
    if results[i]:
      dls[i] = results[i]['dl']
      parses[i] = results[i]['parse']
    else:
      dls[i] = trivial
    normalized_dls[i] = dls[i] / trivial
  return keys, values, dls, normalized_dls, parses

def write_results(mutation_file='data/mutation.mla18ss.fasta',
                  original_file='data/original.mla18ss.fasta',
                  original_aa_file='data/original.aa.fasta',
                  machine=constants.best_mla18ss, output='data/features.csv'):
  mla18ss_w2v_model = gensim.models.word2vec.Word2Vec.load('embeddings.mla18ss')
  aa_w2v_model = gensim.models.word2vec.Word2Vec.load('embeddings.aa')
  window = 10
  n_states = machine._num_states
  # Getting original sequences data.
  aa_sequences = utils.read_fasta(original_aa_file, clean_names=True)
  original_sequences = utils.read_fasta(original_file, clean_names=True)
  # Prepare result data frame.
  cols = ['Protein', 'Position', 'Old', 'New', 'Length', 'Original DL',
          'Mutation DL', 'Delta DL', 'Normalized Original DL',
          'Normalized Mutation DL', 'Normalized Delta DL']
  """cols += list(range(n_states))"""
  cols += list(range(aa_w2v_model.layer1_size * (window * 2 + 1) + mla18ss_w2v_model.layer1_size * window * 2 * 2))
  pandas.DataFrame(columns=cols).to_csv(output, index=False)
  # Getting mutation sequences data. 
  mutation_sequences = utils.read_fasta(mutation_file)
  discrepancies = 0
  # We divide into groups to prevent memory issues.
  counter = 0
  for group in utils.chunks(list(mutation_sequences.keys()), 5000):
    print('Dealing with group', counter)
    counter += 1
    # Verify we have original sequences for each mutation.
    mutation_group = {}
    for key in group:
      protein_name = key.split(',')[0]
      if protein_name not in original_sequences:
        print('WARNING: No original sequence data for', protein_name)
        discrepancies += 1
      else:
        mutation_group[key] = mutation_sequences[key]
    original_group = {k.split(',')[0]: original_sequences[k.split(',')[0]] for k in mutation_group}  
    original_keys, original_seq, original_dls, original_normalized_dls, original_parses =_get_dls(machine, original_group)
    original_seq = dict(zip(original_keys, original_seq))
    original_dls = dict(zip(original_keys, original_dls))
    original_normalized_dls = dict(zip(original_keys, original_normalized_dls))
    original_parses = dict(zip(original_keys, original_parses))
    keys, mutation_seq, dls, normalized_dls, parses = _get_dls(machine, mutation_group)
    result = pandas.DataFrame(columns=cols)
    for i in range(len(keys)):
      protein_name, position, old, new = keys[i].split(',')
      # Sanity check, mutated sequences should have same length as original 
      if len(mutation_seq[i]) != len(original_seq[protein_name]):
        print('WARNING: Sequence discrepancy with', keys[i])
        discrepancies += 1
        continue
      original_parse = original_parses[protein_name]
      mutation_parse = parses[i]
      position = int(position)
      if position < window or position + window >= len(mutation_seq[i]):
        print('WARNING: Mutation position is too close to start or end', keys[i])
        discrepancies += 1
        continue
      features = [protein_name, position, old, new, len(mutation_seq[i]),
                  original_dls[protein_name], dls[i],
                  dls[i] - original_dls[protein_name],
                  original_normalized_dls[protein_name], normalized_dls[i],
                  normalized_dls[i] - original_normalized_dls[protein_name]]  
      """features += parse_analysis.per_state_histogram_delta(original_parse,
                                                           mutation_parse,
                                                           n_states)"""
      aa_original = aa_sequences[protein_name] 
      for model, old_or_new in [[aa_w2v_model, [aa_original]],
                                [mla18ss_w2v_model, [original_seq[protein_name], mutation_seq[i]]]]:
        for seq in old_or_new:
          for offset in range(-window, window):
            letter = seq[offset + position]
            features += [model[letter][k] for k in range(model.layer1_size)]
      features += [aa_w2v_model[new][k] for k in range(aa_w2v_model.layer1_size)]
      result.loc[len(result)+1] = features
    result.to_csv(output, mode='a', header=False, index=False)
  print('Total discrepancies / missing sequences:', discrepancies)

def _get_column_as_list(filename, column_number, parse_as=float):
  with open(filename) as f:
    file_lines = f.readlines()
    file_lines = [line.strip().split(',') for line in file_lines]
    column = []
    for line in file_lines[1:]:
      column.append(parse_as(line[column_number]))
    return column

def _regularize_columns(X):
  # return (X - numpy.min(X, 0)) / (numpy.max(X, 0) - numpy.min(X, 0))
  return (X - numpy.average(X, 0)) / numpy.std(X, 0)

def feature_eval(filenames, machine=constants.best_mla18ss, power=1, column=7):  
  values = []
  for filename in filenames:
    values.append(_get_column_as_list(filename, column))
  whitney = scipy.stats.mannwhitneyu(numpy.power(values[0], power),
                                     numpy.power(values[1], power))
  print('Mann-Whitney U test: {}'.format(whitney[1]))
  print('AUC: {}'.format(1 - whitney[0] / (len(values[0]) * len(values[1]))))
  utils.boxplot(values, 'results/' + filenames[0].split('.')[0])



def _get_features(filenames, cols, regularize=False):
  neutral = numpy.loadtxt(filenames[0],
                          delimiter='\t', skiprows=1, usecols=cols)
  neutral = numpy.hstack((numpy.zeros((len(neutral), 1)), neutral))
  deleterious = numpy.loadtxt(filenames[1],
                              delimiter='\t', skiprows=1, usecols=cols)
  deleterious = numpy.hstack((numpy.ones((len(deleterious), 1)), deleterious))
  # Make sure we have same amount of samples from each kind and combine.
  numpy.random.shuffle(neutral)
  numpy.random.shuffle(deleterious)
  sample_set_size = min(len(neutral), len(deleterious))
  data = numpy.vstack((neutral[0:sample_set_size],
                       deleterious[0:sample_set_size]))
  numpy.random.shuffle(data)
  # Seperate to data and labels.
  X = data[:, 1:]
  if regularize:
    X = _regularize_columns(X)
  y = data[:, 0]
  return X, y

def plot_2_features(filenames, f1, f2):
  import matplotlib.pyplot as plt
  X, y = _get_features(filenames, [f1, f2])
  assert(X.shape[1] == 2)
  assert(y.shape[0] == X.shape[0])
  pos = numpy.where(y == 1)
  neg = numpy.where(y == 0)
  plt.scatter(X[pos, 0], X[pos, 1], marker='o', c='b')
  plt.scatter(X[neg, 0], X[neg, 1], marker='x', c='r')
  plt.xlabel(f1)
  plt.ylabel(f2)
  plt.legend(['Neutral', 'Deleterious'])
  plt.savefig('{}{}.{}vs{}.png'.format(filenames[0], filenames[1], f1, f2))
  plt.close()


def _merge_with_foreign_features(path, needed_features, current, mutation_id,
                                 foreign_data):
  merged = current.merge(foreign_data.get_features(path, needed_features),
                         left_on=mutation_id, right_on=foreign_data.mutation_id)
  for col in foreign_data.mutation_id:
    del merged[col]
  return merged


def _get_all_features_per_protein(features_file,
                                  filenames,
                                  polyphen_features,
                                  provean_features,
                                  sift_features,
                                  features,
                                  nrows,
                                  equal_classes=False,
                                  randomize_classes=False):
  # Define needed columns from CSV according to requested features.
  cols = set([0, 1, 2, 3]) # Protein name, old, new, position
  for i in features:
    cols.add(i)
  # Read the columns and process the data. 
  print('getting cols', cols)
  features_file = pandas.read_csv(features_file, usecols=cols)
  data = []
  for i in range(len(filenames)): # Should be 2
    current = pandas.read_csv(filepath_or_buffer=filenames[i], nrows=nrows)
    mutation_id = ['Protein', 'Position', 'Old', 'New']
    current = current.merge(features_file, on=mutation_id)
    # Add features from other methods.
    dataset_label = filenames[i].split('.')[0] + '.' + filenames[i].split('.')[1]
    if len(polyphen_features) > 0:
      current = _merge_with_foreign_features(dataset_label + '.polyphen.tsv',
                                             polyphen_features, current,
                                             mutation_id, polyphen)
    if len(provean_features) > 0:
      current = _merge_with_foreign_features(dataset_label + '.provean.tsv',
                                             provean_features, current,
                                             mutation_id, provean)
    if len(sift_features) > 0:
      current = _merge_with_foreign_features(dataset_label + '.sift.tsv',
                                             sift_features, current,
                                             mutation_id, sift)
    if randomize_classes:
      current['Class'] = numpy.random.randint(0, 2, size=len(current))
    else:
      current['Class'] = i
    data.append(current)
  # Take equal amounts of mutations
  if equal_classes:
    smaller = min(len(data[0]), len(data[1]))
    for i in range(2):
      current = current.drop(current.index[smaller:])
  # Print column names for convenience
  # print(current.columns)
  # Return the data grouped by protein.
  together = pandas.concat(data)
  return together.groupby('Protein')


humsavar_paths=['data/humsavar/humsavar.neutral.tsv',
                'data/humsavar/humsavar.deleterious.tsv']
humvar_paths=['data/humvar/humvar.neutral.tsv',
              'data/humvar/humvar.deleterious.tsv']
humdiv_paths=['data/humdiv/humdiv.neutral.tsv',
              'data/humdiv/humdiv.deleterious.tsv']



def evaluate(paths=humsavar_paths, experiment_name='last_evaluation',
             n_folds=10, test_allowed_subset=None, polyphen_features=[],
             features=range(4, 255), provean_features=[],
             sift_features=[], algo=4, nrows=None, randomize_classes=False,
             at_least=0, features_file='data/features.csv'):
  print(experiment_name)
  per_protein_samples = _get_all_features_per_protein(
      features_file=features_file,
      provean_features=provean_features, features=features,
      polyphen_features=polyphen_features, sift_features=sift_features,
      filenames=paths, nrows=nrows, randomize_classes=randomize_classes)
  protein_names = []
  for k, _ in per_protein_samples:
    protein_names.append(k)
  random.shuffle(protein_names)
 
 # if at_least > 0: 
 #   def more_than(dataset, at_least=at_least):
 #     # TODO: change to read from csv
 #     data = utils.read_fasta(dataset)
 #     names = list([x.split('\t')[0] for x in data.keys()])
 #     histo = {}
 #     for name in names:
 #       if name not in histo: histo[name] = 0
 #       histo[name] += 1
 #     return set([x for x in names if histo[x] >= at_least])
 #   protein_names = more_than('data/humsavar/humsavar.neutral.mla18ss.fasta') & more_than('data/humsavar/humsavar.deleterious.mla18ss.fasta') & set(protein_names)

  # 10 fold cross validation
  all_probs = []
  all_labels = []
  c = 0
  imp = Imputer(missing_values='NaN', strategy='mean', axis=0)
  for train_ind, test_ind in KFold(len(protein_names), n_folds=n_folds):
    print('Cross validation, iteration {}, {} train proteins and {} test'.format(c, len(train_ind), len(test_ind)))
    c += 1
    train_protein_names = [protein_names[i] for i in train_ind]
    train = pandas.concat([per_protein_samples.get_group(name) for name in train_protein_names]).values[:, 4:]
    test_protein_names = [protein_names[i] for i in test_ind]
    test = pandas.concat([per_protein_samples.get_group(name) for name in test_protein_names]).values
    if test_allowed_subset:
      assert(len(test_allowed_subset) > 0)
      test = numpy.array([x for x in test if (x[0], x[1], x[2], x[3]) in test_allowed_subset])
    test = test[:, 4:]
    test_X = test[:, :-1].astype(numpy.float64)
    test_X = imp.fit_transform(test_X)
    # If only one feature exists, use it as the final score for classification
    # without training.
    if test_X.shape[1] == 1:
      probs = [x[0] for x in test_X.tolist()]
    else:
      train_X = train[:, :-1].astype(numpy.float64)
      train_X = imp.fit_transform(train_X)
      train_y = train[:, -1].astype('int')
      probs = classifiers.classify(train_X, train_y, test_X, algo)
    all_probs += list(probs)
    test_y = test[:, -1].astype('int')
    all_labels += list(test_y)
  auc = pyroc.get_auc(all_labels, all_probs)
  if auc < 0.5:
    print('WARNING: AUC is below 0.5:', auc)
    all_probs = [1 - x for x in all_probs]
    auc = pyroc.get_auc(all_labels, all_probs)
  print('AUC: {}'.format(auc))
  pyroc.plot_roc(all_labels, all_probs, 'classifier_results/' + experiment_name + ' ROC ')
  sys.stdout.flush()
  return all_labels, all_probs

# Methods to evaluate classifier on different dataset:

def get_all_data_(paths, nrows=None, features_file='data/features.csv'):
  per_protein_samples = _get_all_features_per_protein(
      features_file=features_file, provean_features=[], features=[], polyphen_features=[],
      sift_features=[], filenames=paths, nrows=nrows)
  protein_names = []
  for k, _ in per_protein_samples:
    protein_names.append(k)
  all_data = pandas.concat([per_protein_samples.get_group(protein_name) for protein_name in protein_names])
  all_data = all_data.values[:, 4:]
  print('First two training records looks like this:', all_data[0:3])
  all_data_X = all_data[:, :-1].astype(numpy.float64)
  imp = Imputer(missing_values='NaN', strategy='mean', axis=0)
  all_data_X = imp.fit_transform(all_data_X)
  all_data_y = all_data[:, -1].astype('int')
  return all_data_X, all_data_y

def get_best_classifier(nrows, paths):
  all_data_X, all_data_y = get_all_data_(paths=paths, nrows=nrows)
  return classifiers.get_classifier(all_data_X, all_data_y)

def test_model(model, paths, nrows=None):
  all_data_X, all_data_y = get_all_data_(paths=paths, nrows=nrows)
  all_labels = list(all_data_y)
  all_probs = list(model.predict_proba(all_data_X)[:, 1])
  return pyroc.get_auc(all_labels, all_probs)
