
import matplotlib.pyplot as plt

from sklearn.metrics import roc_auc_score
from sklearn.metrics import roc_curve

def get_auc(labels, values):
  return roc_auc_score(labels, values)

def plot_roc(labels, values, filename=None):
  plot_multiple_roc([(labels, values, filename)], filename)

def plot_multiple_roc(labels_values_name_array, filename=None):
  plt.figure()
  for labels, values, name in labels_values_name_array:
    fpr, tpr, _ = roc_curve(labels, values)
    plt.plot(fpr, tpr,
             label='{} (area = {:.2f})'.format(name, get_auc(labels, values)))
  plt.plot([0, 1], [0, 1], 'k--')
  plt.xlim([0.0, 1.0])
  plt.ylim([0.0, 1.0])
  plt.xlabel('False Positive Rate')
  plt.ylabel('True Positive Rate')
  # plt.title('Receiver operating characteristic')
  plt.legend(loc='best')
  if filename:
    plt.savefig(filename)
