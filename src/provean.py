import analysis
import classifiers

import pandas

directory = 'data/humvar_and_humdiv/provean/'

suffix = '.tsv'

paths = [[directory + 'humdiv_neutral' + suffix,
          directory + 'humdiv_deleterious' + suffix],
         [directory + 'humvar_neutral' + suffix ,
          directory + 'humvar_deleterious' + suffix]]

mutation_id = ['PROTEIN_ID', 'POSITION', 'RESIDUE_REF', 'RESIDUE_ALT']

def find_unsure():
  unsure_proteins = set()
  unsure_mutations = set()
  fields = ['PROTEIN_ID','POSITION', 'RESIDUE_REF', 'RESIDUE_ALT']
  for db in paths:
    neutral = pandas.read_csv(filepath_or_buffer=db[0], delimiter='\t')
    deleterious = pandas.read_csv(filepath_or_buffer=db[1], delimiter='\t')

    unsure_deleterious = deleterious[(-3 < deleterious.SCORE) & (deleterious.SCORE < -2)]
    unsure_neutral = neutral[(-3 < neutral.SCORE) & (neutral.SCORE < -2)]

    def add_unsure(x):
      unsure_mutations.add((x.PROTEIN_ID, x.POSITION, x.RESIDUE_REF, x.RESIDUE_ALT))
      unsure_proteins.add(x.PROTEIN_ID)

    for unsure in [unsure_neutral, unsure_deleterious]:
      unsure.apply(axis=1, func=add_unsure)
  return unsure_mutations

def find_mistakes():
  mistakes_mutations = set()
  mistakes_proteins = set()
  fields = ['PROTEIN_ID','POSITION', 'RESIDUE_REF', 'RESIDUE_ALT']
  for db in paths:
    neutral = pandas.read_csv(filepath_or_buffer=db[0], delimiter='\t')
    deleterious = pandas.read_csv(filepath_or_buffer=db[1], delimiter='\t')
    
    false_positives = neutral[neutral['PREDICTION (cutoff=-2.5)'] == 'Deleterious']
    false_negatives = deleterious[deleterious['PREDICTION (cutoff=-2.5)'] == 'Neutral']    

    def add_mistake(x):
      mistakes_mutations.add((x.PROTEIN_ID, x.POSITION, x.RESIDUE_REF, x.RESIDUE_ALT))
      mistakes_proteins.add(x.PROTEIN_ID)

    for mistakes in [false_negatives, false_positives]:
      mistakes.apply(axis=1, func=add_mistake)
  return mistakes_mutations

def get_features(path, required_features):
  return pandas.read_csv(usecols=required_features + mutation_id,
                         filepath_or_buffer=path, delimiter='\t')